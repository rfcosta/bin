#!/usr/bin/perl

BEGIN { push (@INC, "$ENV{HOME}/scripts");
        push (@INC, "$ENV{HOME}/svn.devnet/report/src");
      };
use strict;
use IO::Handle;
use Data::Dumper;
use DB_Util;

my (@rows);

my $QUERY = 'SELECT in_msg_count, method_name  FROM METRIC_L2_20130101 M where method_name like "SSG%" limit 10;';

my $dbu = new DB_Util('prod');

my @ROWS = $dbu->execSqlCmd($QUERY);

my $c = 0;
foreach my $hashOfRow (@ROWS) {
  printf "Row = %3d ", $c++;
  foreach my $field (keys %$hashOfRow) {
     printf "%s = %s, ", $field, $hashOfRow->{$field};
  };
  print "\n";
}

$dbu->close();

exit;

