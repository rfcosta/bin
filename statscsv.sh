#!/bin/sh

for i in ICEMETRC.*.{csv,sorted}
do
   cut -d , -f 7 $i | awk -F. -v file="$i" 'BEGIN{printf " ----- File %s -----\n", file}{if ($2 == "0") { ++milizero } else { ++milinonzero; printf "%s: nonzero=%s\n", file, $0 }};END{printf "%s: Zeroes = %d, Non zeroes = %d\n", file, milizero, milinonzero}'
done

