#!/bin/sh

# This script rotate already loaded data for configtool
# first it finds all directories dist.*/csvdata
# then renames the subdirectoty csvdata/loaded to csvdata/loaded.<YESTERDAY's DATE>
# 
#   `date +%Y%m%d%H%M%S`


YESTERDAY=$(perl -e '@D=localtime(time() -24*60*60 );$D[5]+=1900;$D[4]++;printf("%4d-%02d-%02d",@D[ 5,4,3])')

cd
for DIR in dist.*/csvdata
do
    cd $DIR
    if [ -d loaded ] ; then
         if [ ! -d loaded.$YESTERDAY ] ; then
              mv -v loaded loaded.$YESTERDAY
              mkdir loaded
         else
              echo "Directory $PWD/loaded.$YESTERDAY already there !! Wrong time to run $0"
         fi
    else
         echo "Directory $PWD/loaded not found !! Something is wrong"
    fi
    cd -
done


