#!/bin/sh

set -x
DISK=${1:-910PRGN-SP4-LIN}

hdiutil makehybrid -iso -joliet -o $HOME/Desktop/$DISK.iso /Volumes/$DISK

set -
exit

