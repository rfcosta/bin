#!/bin/sh

pathmunge () {
	if ! echo $PATH | /usr/bin/egrep -q "(^|:)$1($|:)" ; then
	   if [ "$2" = "after" ] ; then
	      PATH=$PATH:$1
	   else
	      PATH=$1:$PATH
	   fi
	fi
}


ldpathmunge () {
	if ! echo $DYLD_LIBRARY_PATH | /usr/bin/egrep -q "(^|:)$1($|:)" ; then
	   if [ "$2" = "after" ] ; then
	      DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$1
	   else
	      DYLD_LIBRARY_PATH=$1:$DYLD_LIBRARY_PATH
	   fi
	fi
}

#Xnest :1 -geometry 1152x870 -query 192.168.1.40
HOSTNAME=`uname -n`
HOSTNAME=${HOSTNAME%%.*}

if [ ${#PS1} -eq 18 ] ; then
   export PS1='[$HOSTNAME] ${PWD##/*/}> '
else
   export PS1='[$HOSTNAME] $PWD> '
fi
alias prompt >/dev/null 2>&1 || alias prompt='. ~/zprompt.sh '


set -o vi
export EDITOR=vi
export FCEDIT=vi
export ZPROMPTLVL=$SHLVL
export oracleLinux5=Unknown000C291844E1

alias mysql=/usr/local/mysql/bin/mysql
#alias purge='echo "Purge is not working on 10.8.1"'
#alias purge='/usr/bin/purge 2> /dev/null; echo "Inactive Memory Purged."'
alias purge='/usr/bin/purge 2>&1; echo "Inactive Memory Purged."'
alias mysqladmin=/usr/local/mysql/bin/mysqladmin
alias dos2unix='perl -pi.dos -e "s/\r\n/\n/"'
alias unix2dos='perl -pi.unix -e "s/\n/\r\n/"'
alias hardware='system_profiler SPHardwareDataType'
alias airport='/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I'
alias sha1='openssl sha1'
alias ls='ls -G'
alias ping='ping -c 5'

[ -f ~/LS_COLORS ] && export LS_COLORS=`cat ~/LS_COLORS`

instantclient=/Applications/instantclient_10_2
subversiondir=/opt/subversion/bin
developerbin=/Applications/Xcode.app/Contents/Developer/usr/bin
developerbin=/Applications/Xcode5-DP2.app/Contents/Developer/usr/bin

pathmunge   ~/bin
pathmunge   $instantclient
pathmunge   $subversiondir
pathmunge   $developerbin


#ldpathmunge $instantclient
#ldpathmunge   $subversiondir

alias sqlplus='~/bin/sqlplus.sh'

export PATH
export DYLD_LIBRARY_PATH

alias sqlp='sqlplus eamconfig/eamconfig@hpblade02-vip.dev.sabre.com:1521/cto11gR2dev'
alias sqlp1='sqlplus eamconfig/eamconfig@hpblade02-vip.dev.sabre.com:1521/cto11gR2dev'
alias sqlp2='sqlplus eamconfig/eamconfig@hpblade10-vip.dev.sabre.com:1521/cto11gR2dev'
alias sqlh='sqlplus system/3104132@192.168.1.17/xe'
alias sqlh='sqlplus regi/just4get@192.168.1.17/xe'
alias sqlh='sqlplus hr/oracle@ORACLE:1521/orcl'
alias sqlv='sqlplus root/3104132@Win7VmW:1521/users'
alias sqlp      >/dev/null 2<&1 || alias sqlp='sqlplus eamconfig/eamconfig@hpblade02-vip.dev.sabre.com:1521/cto11gR2dev'
alias sqlc      >/dev/null 2<&1 || alias sqlc='sqlplus eamconfig/eamconfig@sdfhlc101-vip.sabre.com:1521/CTO11R1C_BASIC'
alias sqll      >/dev/null 2<&1 || alias sqll='sqlplus eamconfiglab/eamconfiglab@ctodb-cluster2.dev.sabre.com:1521/eamconfiglab_cto11g2d'

alias ctovm1827 >/dev/null 2<&1 || alias ctovm1827='ssh prgnusr@ctovm1827.dev.sabre.com'
alias ctovm1161 >/dev/null 2<&1 || alias ctovm1161='ssh visiuser@ctovm1827.dev.sabre.com'
alias eamhlc001 >/dev/null 2<&1 || alias eamhlc001='ssh prgnusr@emhlc001.sabre.com'

alias sqlregi='mysql --host=recanto.local --user=regi --password=just4get --database=EO_summary'


unset pathmunge
unset ld pathmunge


