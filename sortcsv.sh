#!/bin/sh
# for i in ICEMETRC.*.csv;do  cut -d , -f 1-7,9-400 $i | sort > sorted/$i; done
for dir in ~/sabre/submetrics.current/logs ~/sabre/submetrics.notibco/logs
do
    cd $dir
    for i in ICEMETRC.*.csv
    do 
       cut -d , -f 1-7,9-400 $i | sort > ../sorted/${i%%.csv}.sorted
       echo "$PWD/../sorted/${i%%.csv}.sorted "
    done
    cd -
done

