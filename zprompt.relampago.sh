#!/bin/sh

pathmunge () {
	if ! echo $PATH | /usr/bin/egrep -q "(^|:)$1($|:)" ; then
	   if [ "$2" = "after" ] ; then
	      PATH=$PATH:$1
	   else
	      PATH=$1:$PATH
	   fi
	fi
}


ldpathmunge () {
	if ! echo $DYLD_LIBRARY_PATH | /usr/bin/egrep -q "(^|:)$1($|:)" ; then
	   if [ "$2" = "after" ] ; then
	      DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$1
	   else
	      DYLD_LIBRARY_PATH=$1:$DYLD_LIBRARY_PATH
	   fi
	fi
}

#Xnest :1 -geometry 1152x870 -query 192.168.1.40
HOSTNAME=`uname -n`
HOSTNAME=${HOSTNAME%%.*}

if [ ${#PS1} -eq 18 ] ; then
   export PS1='[$HOSTNAME] ${PWD##/*/}> '
else
   export PS1='[$HOSTNAME] $PWD> '
fi
alias prompt    >/dev/null 2>&1 || alias prompt='. ~/bin/zprompt.sh '
alias macsvn='/Applications/Xcode.app/Contents/Developer/usr/bin/svn'
alias tree='find . -print | sed -e "s;[^/]*/;|____;g;s;____|; |;g"'
alias ping='ping -c 3 '


set -o vi
export EDITOR=vi
export FCEDIT=vi
export ZPROMPTLVL=$SHLVL
#alias purge='echo "Purge is not working on 10.8.1"'
#alias purge='/usr/bin/purge 2> /dev/null; echo "Inactive Memory Purged."'
alias purge='/usr/bin/purge 2>&1; echo "Inactive Memory Purged."'
alias dos2unix='perl -pi.dos -e "s/\r\n/\n/"'
alias unix2dos='perl -pi.unix -e "s/\n/\r\n/"'
alias hardware='system_profiler SPHardwareDataType'
alias airport='/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I'
alias sha1='openssl sha1'
alias ls='ls -G'
#alias uex=/Applications/UltraEdit.app/Contents/MacOS/UltraEdit
[ -x ~/bin/uex.sh ] && alias uex=~/bin/uex.sh

[ -f ~/LS_COLORS ] && export LS_COLORS=`cat ~/LS_COLORS`

export JAVA_HOME6=/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home
export JAVA_HOME7=/Library/Java/JavaVirtualMachines/jdk1.7.0_15.jdk/Contents/Home
export JAVA_HOME=$JAVA_HOME6

instantclient=~/instantclient_10_2
svnclient=/opt/subversion/bin
developerbin=/Applications/Xcode.app/Contents/Developer/usr/bin
javabin=$JAVA_HOME/bin



pathmunge   $instantclient
pathmunge   $svnclient
pathmunge   $developerbin
pathmunge   $javabin
pathmunge   ~/bin
#Mountain lion is warning about DYLD variables so I encapsulated this in ~/sqlplus.sh  script
#ldpathmunge $instantclient 

export PATH
export DYLD_LIBRARY_PATH

alias sqlplus='~/bin/sqlplus.sh' # Run sqlplus under another shell level to define DYLD variables
#alias sqlh  >/dev/null 2<&1 || alias sqlh='sqlplus eamconfig/eamconfig@192.168.1.17:1521/xe'
alias sqlh  >/dev/null 2<&1 || alias sqlh='sqlplus HR/oracle@oraclew5.local:1521/orcl'
alias sqll  >/dev/null 2<&1 || alias sqll='sqlplus eamconfiglab/eamconfiglab@ctodb-cluster2.dev.sabre.com:1521/eamconfiglab_cto11g2d'
alias sqld  >/dev/null 2<&1 || alias sqlp='sqlplus eamconfig/eamconfig@ctodb-cluster2.dev.sabre.com:1521/cto11gR2dev'
alias sqlp  >/dev/null 2<&1 || alias sqlp='sqlplus eamconfig/eamconfig@ctodb-cluster2.dev.sabre.com:1521/cto11gR2dev'
alias sqlc  >/dev/null 2<&1 || alias sqlc='sqlplus eamconfig/eamconfig@sdfhlc101-vip.sabre.com:1521/CTO11R1C_BASIC'
alias sqlprod >/dev/null 2<&1 || alias sqlprod='sqlplus eamconfig/EAMCONFIG@sdfhlp201-vip.sabre.com:1521/CTO11R1P_BASIC'
alias mysql=/usr/local/mysql-5.5.25-osx10.6-x86_64/bin/mysql
alias clrr='mysql -t --host=icehlp901.sabre.com --port=3306 --user=iceread --password=iceread --database=fabric_metric_prod'
alias clrw='mysql -t --host=icehlp901.sabre.com --port=3306 --user=clradmin --password=clradmin1 --database=fabric_metric_prod'

default_user=prgnusr
for host in ctovm1827 prgnr08 ctovm054 visiuser@ctovm1161 eamusr@ltxl0200 ; do
    server=${host#*@}
    [ "$server" = "$host" ] && host=${default_user}@$host
    #alias ${server} >/dev/null 2<&1 || \
     alias ${server}="ssh ${host}.dev.sabre.com"
done

for host in sg549743@fsehlp04 icehld701 eamhlc001 eamhlp001 eamhlp002 eamhlp003 ; do
    server=${host#*@}
    [ "$server" = "$host" ] && host=${default_user}@$host
    #alias ${server} >/dev/null 2<&1 || \
     alias ${server}="ssh ${host}.sabre.com"
done


unset pathmunge
unset ld pathmunge


