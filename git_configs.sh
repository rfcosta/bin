#!/bin/sh

git=/Applications/Xcode5-DP2.app/Contents/Developer/usr/bin/git

$git config --global user.name "Reginaldo Costa"
$git config --global user.email "Reginaldo.Costa@sabre.com"
$git config --global color.ui true
$git config --global alias.lol "log --oneline --graph --decorate"

# $git log --oneline
# $git log --oneline --graph
# $git log --oneline --graph --decorate

