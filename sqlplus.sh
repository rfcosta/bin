#!/bin/sh

ldpathmunge () {
         if ! echo $DYLD_LIBRARY_PATH | /usr/bin/egrep -q "(^|:)$1($|:)" ; then
            if [ "$2" = "after" ] ; then
               export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$1
            else
               export DYLD_LIBRARY_PATH=$1:$DYLD_LIBRARY_PATH
            fi
         fi
}


#instantclient=/Users/regi/instantclient_10_2
instantclient=/usr/bin
#ldpathmunge $instantclient
unset ldpathmunge

$instantclient/sqlplus $*

