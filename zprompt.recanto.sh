#!/bin/sh

pathmunge () {
	if ! echo $PATH | /usr/bin/egrep -q "(^|:)$1($|:)" ; then
	   if [ "$2" = "after" ] ; then
	      PATH=$PATH:$1
	   else
	      PATH=$1:$PATH
	   fi
	fi
}


ldpathmunge () {
	if ! echo $DYLD_LIBRARY_PATH | /usr/bin/egrep -q "(^|:)$1($|:)" ; then
	   if [ "$2" = "after" ] ; then
	      DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$1
	   else
	      DYLD_LIBRARY_PATH=$1:$DYLD_LIBRARY_PATH
	   fi
	fi
}

#Xnest :1 -geometry 1152x870 -query 192.168.1.40
HOSTNAME=`uname -n`
HOSTNAME=${HOSTNAME%%.*}

if [ ${#PS1} -eq 18 ] ; then
   export PS1='[$HOSTNAME] ${PWD##/*/}> '
else
   export PS1='[$HOSTNAME] $PWD> '
fi
alias prompt    >/dev/null 2>&1 || alias prompt='. ~/zprompt.sh '
alias macsvn='/Applications/Xcode.app/Contents/Developer/usr/bin/svn'
alias unlockfiles='chflags -R nouchg *'
alias dblank="sed -i .dblank  '/^$/d' "
alias delb='perl -pi.delb -e "s{^\s*\n$}{}" '
alias ping='ping -c 3 '


set -o vi
export EDITOR=vi
export FCEDIT=vi
export ZPROMPTLVL=$SHLVL
#alias purge='echo "Purge is not working on 10.8.1"'
#alias purge='/usr/bin/purge 2> /dev/null; echo "Inactive Memory Purged."'
alias purge='/usr/bin/purge 2>&1; echo "Inactive Memory Purged."'
alias dos2unix='perl -pi.dos -e "s/\r\n/\n/"'
alias unix2dos='perl -pi.unix -e "s/\n/\r\n/"'
alias hardware='system_profiler SPHardwareDataType'
alias airport='/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I'
alias sha1='openssl sha1'
alias ls='ls -G'
alias vpn='open /Applications/Cisco/Cisco\ AnyConnect\ Secure\ Mobility\ Client.app'
#alias uex=/Applications/UltraEdit.app/Contents/MacOS/UltraEdit
[ -x ~/bin/uex.sh ] && alias uex=~/bin/uex.sh

[ -f ~/LS_COLORS ] && export LS_COLORS=`cat ~/LS_COLORS`

instantclient=~/instantclient_10_2
svnclient=/opt/subversion/bin
svnclient=/Applications/Xcode5-DP2.app/Contents/Developer/usr/bin

pathmunge   $instantclient
pathmunge   $svnclient
pathmunge   ~/bin
#Mountain lion is warning about DYLD variables so I encapsulated this in ~/sqlplus.sh  script
#ldpathmunge $instantclient 

export PATH
export DYLD_LIBRARY_PATH

alias sqlplus='~/bin/sqlplus.sh' # Run sqlplus under another shell level to define DYLD variables
alias eiconc='sqlplus  prgnusr/wh51kge9@sachlc100-vip.sabre.com:1521/EYECONC'
alias eiconp='sqlplus  prgnusr/wh51kge9@sachlp100-vip.sabre.com:1521/EYECONP'
alias eiconp0='sqlplus prgnusr/wh51kge9@sachlc100-vip.sabre.com:1521/EYECONC'
alias eiconp1='sqlplus prgnusr/wh51kge9@sachlc101-vip.sabre.com:1521/EYECONC'
alias eiconp0='sqlplus prgnusr/wh51kge9@sachlp100-vip.sabre.com:1521/EYECONP'
alias eiconp1='sqlplus prgnusr/wh51kge9@sachlp101-vip.sabre.com:1521/EYECONP'
alias eiconp2='sqlplus prgnusr/wh51kge9@sachlp102-vip.sabre.com:1521/EYECONP'


alias eiconc='sqlplus  prgnusr/wh51kge9@sachlc100-vip.sabre.com:1521/EYECONC'
alias eiconc="sqlplus prgnusr/wh51kge9@'(DESCRIPTION=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=sachlc100-vip.sabre.com)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=sachlc101-vip.sabre.com)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=EYECONC)(SERVER=DEDICATED)))'"
alias eiconp="sqlplus 'prgnusr/wh51kge9@(DESCRIPTION=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=sachlp100-vip)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=sachlp101-vip)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=sachlp102-vip)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=EYECONP)(SERVER=DEDICATED)))'"
alias eiconp0='sqlplus prgnusr/wh51kge9@sachlc100-vip.sabre.com:1521/EYECONC'
alias eiconp1='sqlplus prgnusr/wh51kge9@sachlc101-vip.sabre.com:1521/EYECONC'
alias eiconp0='sqlplus prgnusr/wh51kge9@sachlp100-vip.sabre.com:1521/EYECONP'
alias eiconp1='sqlplus prgnusr/wh51kge9@sachlp101-vip.sabre.com:1521/EYECONP'
alias eiconp2='sqlplus prgnusr/wh51kge9@sachlp102-vip.sabre.com:1521/EYECONP'


alias sqlh  >/dev/null 2<&1 || alias sqlh='sqlplus HR/oracle@oraclew5.local:1521/orcl'
alias sqlx  >/dev/null 2<&1 || alias sqlx='sqlplus REGI/REGI@XORACLE.local:1521/xe'
alias sqll  >/dev/null 2<&1 || alias sqll='sqlplus eamconfiglab/eamconfiglab@ctodb-cluster2.dev.sabre.com:1521/eamconfiglab_cto11g2d'
alias sqld  >/dev/null 2<&1 || alias sqlp='sqlplus eamconfig/eamconfig@ctodb-cluster2.dev.sabre.com:1521/cto11gR2dev'
alias sqlp  >/dev/null 2<&1 || alias sqlp='sqlplus eamconfig/eamconfig@ctodb-cluster2.dev.sabre.com:1521/cto11gR2dev'
alias sqlc  >/dev/null 2<&1 || alias sqlc='sqlplus eamconfig/eamconfig@sdfhlc101-vip.sabre.com:1521/CTO11R1C_BASIC'
alias sqlprod >/dev/null 2<&1 || alias sqlprod='sqlplus eamconfig/EAMCONFIG@sdfhlp201-vip.sabre.com:1521/CTO11R1P_BASIC'
alias mysql=/usr/local/mysql-5.5.25-osx10.6-x86_64/bin/mysql
alias mysqladmin=/usr/local/mysql-5.5.25-osx10.6-x86_64/bin/mysqladmin
alias clrr='mysql -t --host=icehlp901.sabre.com --port=3306 --user=iceread --password=iceread --database=fabric_metric_prod'
alias clrw='mysql -t --host=icehlp901.sabre.com --port=3306 --user=clradmin --password=clradmin1 --database=fabric_metric_prod'


alias sqlregi='mysql --host=recanto.local --user=regi --password=just4get --database=EO_summary'
alias sqlregit='mysql --host=recanto.local --user=regi --password=just4get --database=test'

default_user=prgnusr
default_domain=dev.sabre.com
for host in ctovm1827 prgnr08 regi@ctovm054 visiuser@ctovm1161 eamusr@ltxl0200 ctovm193 ctovm582; do
    server=${host#*@}
    if [ "$server" = "$host" ] ; then
        host=${default_user}@$host
    fi
    alias ${server} >/dev/null 2<&1 || alias ${server}="ssh ${host}.${default_domain}"
done

default_domain=sabre.com
for host in sg549743@fsehlp04 prihlc01 prihlp02 icehld701 eamhlc001 eamhlp001 eamhlp002 eamhlp003 ; do
    server=${host#*@}
    if [ "$server" = "$host" ] ; then
        host=${default_user}@$host
    fi
    alias ${server} >/dev/null 2<&1 || alias ${server}="ssh ${host}.${default_domain}"
done

default_user=`whoami`
default_domain=local
for host in oracle@oraclew5 relampago faisca ; do
    server=${host#*@}
    if [ "$server" = "$host" ] ; then
        host=${default_user}@$host
    fi
    alias ${server} >/dev/null 2<&1 || alias ${server}="ssh ${host}.${default_domain}"
done




unset pathmunge
unset ld pathmunge


