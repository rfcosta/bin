package DB_Util;


use strict;
use DBI;

our (@ISA, @EXPORT);
BEGIN {
     require Exporter;
     @ISA = qw(Exporter);
     @EXPORT = qw( now trim yesterday 
                   tokenizeCSV TimeStampCalcHours
                   TimeStampCalcMinutes
                 );
}


sub new {

   my $type = shift;
   my $class = ref( $type ) || $type;

   my ( $env
      ) = @_;
   
   $env = undef unless ($env == 'cert' || $env == 'prod');
   my $host = '';
   if ($env == 'prod' ) { 
        $host =  'icehlp903.sabre.com' 
   } else {
        $host = 'icehlc801.sabre.com';
   };
   die "Invalid environment" unless $env;
       	     

   my $self =  { env          => $env ? $env : 'cert' # Environment
               , dbhost       => $host
               , dbport       => 3306
               , dbuser       => 'iceread'
               , dbpass       => 'iceread'
               , database     => "fabric_metric_$env"
               };

   bless $self, $class;
   
   $self->{DBH} = $self->dbinit();        # Database handle
   return $self;

};

sub dbinit {
     my $self = shift;
     
     my $host = $self->{dbhost};
     my $port = $self->{dbport};
     my $user = $self->{dbuser};
     my $pass = $self->{dbpass};
     $pass = '' if $pass eq "NULL";
     my $dsn = "DBI:mysql:database=$self->{database};host=$host;port=$port";
     my $dbh = DBI->connect( $dsn
                           , $user
                           , $pass
                           , {  RaiseError => 0
                             ,  AutoCommit => 0
                             ,  PrintError => 0
                             }
                           )
               or die ("Database Error: $DBI::errstr")
               ;
               
};


sub close {
     my $self = shift;
     
     my $dbh = $self->{DBH};
     $dbh->disconnect() if defined $dbh;
     undef $self->{DBH};
     
}

sub execSqlCmd {
     my $self = shift;
     my $sqlCommands = shift;

     my $dbh = $self->{DBH};

     return 0 unless (defined $dbh && $sqlCommands);
     
     my @sqlCmd = split /\;/, $sqlCommands; # Ignore past ';'
     
     my $sth = $dbh->prepare($sqlCmd[0] . ";");
     $sth->execute();
     die "Database Error: $DBI::errstr" unless $sth;

     my @result = ();

     while ( my $fieldHashRef = $sth->fetchrow_hashref() ) {
          push(@result, $fieldHashRef);
     };

     if (wantarray)
     {
          return @result;
     }
     else
     {
          return $result[0];
     };

};

#------------------------------------------------
# Get current date and time
#------------------------------------------------
sub now {
     my ($dayfmt) = @_;
     $dayfmt = "%4d-%02d-%02d" unless $dayfmt; 
     my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
     $year = $year + 1900; ++$mon;
     if (wantarray)
     {
          return (sprintf($dayfmt, $year, $mon, $mday),sprintf("%02d:%02d:%02d", $hour, $min, $sec))
     }
     else
     {
          return sprintf("$dayfmt %02d:%02d:%02d", $year, $mon, $mday, $hour, $min, $sec);
     };
};

#------------------------------------------------
# Get Yesterday's date and time
#------------------------------------------------
sub yesterday {
     my ($dayfmt) = @_;
     $dayfmt = "%4d-%02d-%02d" unless $dayfmt; 
     my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time() - 24*60*60 );
     $year = $year + 1900; ++$mon;
     if (wantarray)
     {
          return (sprintf($dayfmt, $year, $mon, $mday),sprintf("%02d:%02d:%02d", $hour, $min, $sec))
     }
     else
     {
          return sprintf("$dayfmt %02d:%02d:%02d", $year, $mon, $mday, $hour, $min, $sec);
     };
};

#------------------------------------------------
# returns given timestamp plus/minus given hours
#------------------------------------------------
sub TimeStampCalcHours {
     my ($timeStamp, $hours) = @_;
     return TimeStampCalcMinutes($timeStamp, $hours * 60);
};

sub TimeStampCalcMinutes {
use Time::Local;
#
# TimeStamp formats: 2010-12-31 17:45:01 or 
#                    2010-12-31:17:45:01 or 
#                    2010/12/31 17:45:01 or 
#                    2010/12/31:17:45:01
#                 or 12/31/2010 17:45:01 or
#                    12/31/2010:17:45:01 or
#                    12-31-2010 17:45:01 or
#                    12-31-2010:17:45:01 
#
# Hours = +|- <number of hours>
#
     my ($timeStamp, $minutes) = @_;

     my ($sec,$min,$hour,$mday,$mon,$year);
     my $format = 0;

     $minutes = 0 unless $minutes;
    

     my $aaaammddhhmmss = '\s*(\d{4,4})[-/](\d{1,2})[-/](\d{1,2})(\s*|:)(\d{1,2}):(\d\d):(\d\d)';
     my $mmddaaaahhmmss = '\s*(\d{1,2})[-/](\d{1,2})[-/](\d{4,4})(\s*|:)(\d{1,2}):(\d\d):(\d\d)';

     if ($timeStamp =~ /$aaaammddhhmmss/) {
          $format = 1;
          ($sec,$min,$hour,$mday,$mon,$year) = ($7,$6,$5,$3,$2 - 1,$1 - 1900);
     }
     elsif ($timeStamp =~ /$mmddaaaahhmmss/) {
          $format = 2;
          ($sec,$min,$hour,$mday,$mon,$year) = ($7,$6,$5,$2,$1 - 1,$3 - 1900);
     }
     else {
          die "Invalid date format: $timeStamp \n";
     };

     my $outFmt = $timeStamp;
     $outFmt =~ s/\d\d\d\d/\%AAd/g;
     $outFmt =~ s/\d\d/\%BBd/g;
     $outFmt =~ s/\d/\%d/g;
     $outFmt =~ s/AA/04/g;
     $outFmt =~ s/BB/02/g;

     my $time = timelocal($sec,$min,$hour,$mday,$mon,$year) + $minutes * 60;
     ($sec,$min,$hour,$mday,$mon,$year,my $wday, my $yday, my $isdst) = localtime($time);
     $year = $year + 1900; ++$mon;

     my $outTimeStamp = ($format == 1)
                      ? sprintf($outFmt, $year, $mon, $mday, $hour, $min, $sec)
                      : sprintf($outFmt, $mon, $mday, $year, $hour, $min, $sec)
                      ;

     return $outTimeStamp;     

};

#------------------------------------------------
# Remove leading and trailing spaces
#------------------------------------------------
sub trim {
    my $string = shift;
    $string =~ s/^\s+//;
    $string =~ s/\s+$//;
    return $string;
};



sub tokenizeCSV {

     (my $text) = @_;
     my @tokens  = ();
     push(@tokens, $+) while $text =~ m{
        # the first part groups the phrase inside the quotes.
        # see explanation of this pattern in MRE
        "([^\"\\]*(?:\\.[^\"\\]*)*)",?
           |  ([^,]+),?
           | ,
          }gx;
     push(@tokens, undef) if substr($text, -1,1) eq ',';

     return @tokens;
};



1; # Required by PERL => Returns TRUE

